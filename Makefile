CC = gcc
CFLAGS = -Wall -Wextra -Werror -Isrc
LDFLAGS = -pthread

SOURCES_DIR = src
SOURCES = $(wildcard $(SOURCES_DIR)/*.c)

all:
	$(CC) $(CFLAGS) $(LDFLAGS) $(SOURCES) -o main

debug: CFLAGS += -g -O0
debug: clean all

clean:
	$(RM) main
