#ifndef THREADPOOL_H_
#define THREADPOOL_H_

#include <stddef.h>

#define MAX_MESSAGE_LENGTH 1023

typedef void (*threadpool_callback)(char msg[MAX_MESSAGE_LENGTH+1],
    size_t msglen, int fd, void* data, int threadid);

int threadpool_init(threadpool_callback callback);

int threadpool_register_endpoint(int fd, void* data);

int threadpool_unregister_endpoint(int fd);

int threadpool_run();

int threadpool_exit();

#endif // THREADPOOL_H_pthread_t th