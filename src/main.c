#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdint.h>
#include <signal.h>

#include "threadpool.h"

#define ENDPOINTS 4
#define BASE_PORT 5642
#define MAX_MESSAGE_LENGTH 1023

static int sockets[ENDPOINTS];

void callback(char msg[MAX_MESSAGE_LENGTH+1], size_t msglen, int fd, void* data, int threadid) {
    (void) data;
    msg[msglen] = 0;
    printf("(Socket %i, Thread %i) Message is: %s\n", fd, threadid, msg);
    sleep(1);
}

int sock_create(int port)
{
    int sockfd = socket(AF_INET, SOCK_DGRAM, 0);

    if (sockfd < 0)
    {
        fprintf(stderr, "%s: could not initialize socket\n", __FUNCTION__);
        return -1;
    }

    struct sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = inet_addr("0.0.0.0");
    memset(addr.sin_zero, '\0', sizeof(addr.sin_zero));

    int err = bind(sockfd, (struct sockaddr *) &addr, sizeof(addr));
    if (err != 0)
    {
        fprintf(stderr, "%s: could not bind socket\n", __FUNCTION__);
        close(sockfd);
        return -1;
    }

    printf("Created socket %i\n", sockfd);

    return sockfd;
}

void sigTerm(int code) {
    (void) code;
    printf(" Caught stop signal\n");
    threadpool_exit();
    exit(0);
}

int main()
{
    signal(SIGTERM, &sigTerm);
    signal(SIGINT, &sigTerm);

    threadpool_init(callback);

    for(int i = 0; i < ENDPOINTS; i++) {
        int sockfd = sock_create(BASE_PORT + i);
        if(sockfd == -1)
            return 1;
        sockets[i] = sockfd;
        threadpool_register_endpoint(sockfd, NULL);
    }

    threadpool_run();

    // Exit code
    for(int i = 0; i < ENDPOINTS; i++)
        close(sockets[i]);

    return 0;
}