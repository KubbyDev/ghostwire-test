#include <pthread.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <stdlib.h>
#include <errno.h>
#include <signal.h>

#include "threadpool.h"

#define THREADS 16
#define MAX_EVENTS 16

/*

The pool must check the endpoints and assign
a thread to a task when its available
The task must know which endpoint the data comes from

WARNINGS
Packets will arrive out of order (since they are processed by different threads)

TO CHECK
The MAX_MESSAGE_LENGTH might mess up things badly
When calling read we must receive one full packet or everything breaks

*/

static bool isinit = false;
static bool islooping = false;
static int epollfd = -1;
static threadpool_callback callback = NULL;
static sigset_t wakeupsignal;

struct gw_worker {
    pthread_t thread;
    int id;
    bool available; // False when processing a packet
    struct gw_endpoint* endpoint; // When working, assigned endpoint
};

struct gw_endpoint {
    int fd;
    void* data; // User data
    bool available; // False when a packet is being read by a worker
    struct gw_endpoint* next;
};

static struct gw_worker workers[THREADS];
static struct gw_endpoint endpoints = {0}; // endpoints linked list sentinel

void* worker(void* arg) {

    struct gw_worker* worker = (struct gw_worker*) arg;

    while(islooping) {

        // Waits for work
        int sig;
        sigwait(&wakeupsignal, &sig);
        if(!islooping)
            break;

        printf("Thread %i start\n", worker->id);

        struct gw_endpoint* endpoint = worker->endpoint;

        // Reads the packet
        char msg[MAX_MESSAGE_LENGTH+1];
        ssize_t rd = read(endpoint->fd, msg, MAX_MESSAGE_LENGTH);
        __sync_synchronize();
        worker->endpoint->available = true;
        __sync_synchronize();

        if(rd < 0)
            fprintf(stderr, "%s: Read failed with %li\n", __FUNCTION__, rd);
        else
            callback(msg, rd, endpoint->fd, endpoint->data, worker->id);

        printf("Thread %i end\n", worker->id);

        __sync_synchronize();
        worker->available = true;
    }

    return NULL;
}

int schedule_worker(struct gw_endpoint* endpoint) {

    // Finds an available thread
    int workerid = 0;
    while(workerid < THREADS && !(workers[workerid].available))
        workerid++;
    if(workerid == THREADS)
        return 1;

    // If the endpoint is already in use, aborts
    if(!endpoint->available)
        return 1;

    printf("Scheduling thread %i for endpoint %i\n", workerid, endpoint->fd);

    // Starts the task
    struct gw_worker* w = &workers[workerid];
    w->endpoint = endpoint;
    __sync_synchronize();
    w->available = false;
    endpoint->available = false;
    __sync_synchronize();
    pthread_kill(w->thread, SIGUSR1);
    return 0;
}

int threadpool_init(threadpool_callback cb) {

    // Initialises the signals set (used to wake threads up)
    sigemptyset(&wakeupsignal);
    sigaddset(&wakeupsignal, SIGUSR1);
    pthread_sigmask(SIG_BLOCK, &wakeupsignal, NULL);

    // Create the workers
    islooping = true;
    __sync_synchronize(); // Full memory barrier
    for(int i = 0; i < THREADS; i++) {
        struct gw_worker* w = &workers[i];
        w->available = true;
        w->id = i;
        w->endpoint = NULL;
        pthread_create(&(w->thread), NULL, worker, w);
    }

    // Creates an epoll socket
    if ((epollfd = epoll_create1(0)) < 0) {
        fprintf(stderr, "%s: could not create epoll\n", __FUNCTION__);
        islooping = false;
        return 1;
    }

    callback = cb;
    isinit = true;
    return 0;
}

int threadpool_register_endpoint(int fd, void* data) {

    if (!isinit) {
        fprintf(stderr, "%s: threadpool has not been initialized!\n", __FUNCTION__);
        return 1;
    }

    // Initialises the gw_endpoint structure
    struct gw_endpoint* endpoint = malloc(sizeof(struct gw_endpoint));
    if (endpoint == NULL)
        return 1;
    endpoint->fd = fd;
    endpoint->available = true;
    endpoint->data = data;
    // Adds the endpoint to the list
    endpoint->next = endpoints.next;
    endpoints.next = endpoint;

    // Registers the endpoint into epoll
    struct epoll_event ev = {0};
    ev.events = EPOLLIN; // Input stream, level triggered
    ev.data.ptr = endpoint; // gw_endpoint structure
    printf("Epolling endpoint %i\n", fd);
    if (epoll_ctl(epollfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
        fprintf(stderr, "%s: could not add endpoint to interest list errno:%i\n", __FUNCTION__, errno);
        free(endpoint);
        return 1;
    }

    return 0;
}

int threadpool_unregister_endpoint(int fd) {

    if (!isinit) {
        fprintf(stderr, "%s: threadpool has not been initialized!\n", __FUNCTION__);
        return 1;
    }

    // Removes the endpoint from the list
    struct gw_endpoint* e = &endpoints;
    while(e->next) {
        struct gw_endpoint* nxt = e->next;
        if(nxt->fd == fd) {
            e->next = nxt->next;
            free(nxt);
            return 0;
        }
        e = e->next;
    }

    return 1;
}

int threadpool_run() {

    if (!isinit) {
        fprintf(stderr, "%s: threadpool has not been initialized!\n", __FUNCTION__);
        return 1;
    }

    while(islooping) {

        struct epoll_event ev[MAX_EVENTS];
        int nevents = epoll_wait(epollfd, ev, MAX_EVENTS, 50);

        if (nevents < 0) {
            fprintf(stderr, "%s: epoll_wait failed\n", __FUNCTION__);
            return 1;
        }

        for(int i = 0; i < nevents; i++)
            schedule_worker(ev[i].data.ptr);
    }

    close(epollfd);
    return 0;
}

int threadpool_exit() {

    if (!isinit)
        return 0;
    isinit = false;

    printf("Waiting for current tasks to be finished\n");

    islooping = false;
    __sync_synchronize();

    // Joins all the worker's threads
    for(int i = 0; i < THREADS; i++) {
        pthread_kill(workers[i].thread, SIGUSR1);
        pthread_join(workers[i].thread, NULL);
    }

    printf("Exiting threadpool\n");

    // Clears the endpoints list
    struct gw_endpoint* e = endpoints.next;
    while(e) {
        struct gw_endpoint* nxt = e->next;
        free(e);
        e = nxt;
    }

    isinit = false;
    return 0;
}