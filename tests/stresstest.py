import socket

SOCKETS = 4
BASE_PORT = 5642

def send_message(message, sockid):
    print(f"Sending: \"{message}\"")
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.sendto(bytes(message, "utf-8"), ("localhost", BASE_PORT+sockid))

def main():
    for i in range(SOCKETS):
        for j in range(8):
            send_message(f"Hello this is message {i+4*j}", i)

if __name__ == "__main__":
    main()
